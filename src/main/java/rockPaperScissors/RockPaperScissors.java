package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Random ran = new Random();
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(true){
            System.out.println(String.format("Let's play round %s", roundCounter));

            String usersChoice = usersChoice();
            String computersChoice = rpsChoices.get(ran.nextInt(rpsChoices.size()));
            String choices = String.format("Human chose %s, computer chose %s.", usersChoice, computersChoice);

            if(usersChoice.equals(computersChoice)){
                System.out.println(choices + " It's a tie!");
            }else {
                if(findWinner(usersChoice, computersChoice)){
                    humanScore += 1;
                    System.out.println(choices + " Human wins.");
                }else{
                    computerScore += 1;
                    System.out.println(choices + " Computer wins.");
                }
            }

            System.out.println(String.format("Score: human %s, computer %s.", humanScore, computerScore));

            roundCounter += 1;
            String continueAnswer = continuePlaying();
            if(continueAnswer.equals("n")){
                break;
            }
        }

        System.out.println("Bye bye :)");
    }
    public boolean findWinner(String choice_one, String choice_two){
        if(choice_one.equals("rock")){
            return choice_two.equals("scissors");
        }else if(choice_one.equals("scissors")){
            return choice_two.equals("paper");
        }else{
            return choice_two.equals("rock");
        }
    }

    public String usersChoice(){
        while(true){
            String usersChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            if(validate_input(usersChoice, rpsChoices)){
                return usersChoice;
            }else {
                System.out.println(String.format("I don't understand %s. Try again", usersChoice));
            }
        }
    }

    public String continuePlaying(){
        while(true){
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
            if(validate_input(continueAnswer, Arrays.asList("n", "y"))){
                return continueAnswer;
            }else {
                System.out.println(String.format("I don't understand %s. Try again", continueAnswer));
            }
        }
    }

    public boolean validate_input(String userChoice, List<String> choices){
        return choices.contains(userChoice);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput.toLowerCase();
    }

}
